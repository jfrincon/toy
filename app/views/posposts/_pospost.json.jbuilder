json.extract! pospost, :id, :latitud, :longitud, :user_id, :created_at, :updated_at
json.url pospost_url(pospost, format: :json)

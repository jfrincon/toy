class PospostsController < ApplicationController
  before_action :set_pospost, only: [:show, :edit, :update, :destroy]

  # GET /posposts
  # GET /posposts.json
  def index
    @posposts = Pospost.all
  end

  # GET /posposts/1
  # GET /posposts/1.json
  def show
  end

  # GET /posposts/new
  def new
    @pospost = Pospost.new
  end

  # GET /posposts/1/edit
  def edit
  end

  # POST /posposts
  # POST /posposts.json
  def create
    @pospost = Pospost.new(pospost_params)

    respond_to do |format|
      if @pospost.save
        format.html { redirect_to @pospost, notice: 'Pospost was successfully created.' }
        format.json { render :show, status: :created, location: @pospost }
      else
        format.html { render :new }
        format.json { render json: @pospost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posposts/1
  # PATCH/PUT /posposts/1.json
  def update
    respond_to do |format|
      if @pospost.update(pospost_params)
        format.html { redirect_to @pospost, notice: 'Pospost was successfully updated.' }
        format.json { render :show, status: :ok, location: @pospost }
      else
        format.html { render :edit }
        format.json { render json: @pospost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posposts/1
  # DELETE /posposts/1.json
  def destroy
    @pospost.destroy
    respond_to do |format|
      format.html { redirect_to posposts_url, notice: 'Pospost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pospost
      @pospost = Pospost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pospost_params
      params.require(:pospost).permit(:latitud, :longitud, :user_id)
    end
end

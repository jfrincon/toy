require 'test_helper'

class PospostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pospost = posposts(:one)
  end

  test "should get index" do
    get posposts_url
    assert_response :success
  end

  test "should get new" do
    get new_pospost_url
    assert_response :success
  end

  test "should create pospost" do
    assert_difference('Pospost.count') do
      post posposts_url, params: { pospost: { latitud: @pospost.latitud, longitud: @pospost.longitud, user_id: @pospost.user_id } }
    end

    assert_redirected_to pospost_url(Pospost.last)
  end

  test "should show pospost" do
    get pospost_url(@pospost)
    assert_response :success
  end

  test "should get edit" do
    get edit_pospost_url(@pospost)
    assert_response :success
  end

  test "should update pospost" do
    patch pospost_url(@pospost), params: { pospost: { latitud: @pospost.latitud, longitud: @pospost.longitud, user_id: @pospost.user_id } }
    assert_redirected_to pospost_url(@pospost)
  end

  test "should destroy pospost" do
    assert_difference('Pospost.count', -1) do
      delete pospost_url(@pospost)
    end

    assert_redirected_to posposts_url
  end
end

Rails.application.routes.draw do
  resources :posposts
  resources :users
  root 'users#index'
end

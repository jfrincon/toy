class CreatePosposts < ActiveRecord::Migration[5.1]
  def change
    create_table :posposts do |t|
      t.float :latitud
      t.float :longitud
      t.integer :user_id

      t.timestamps
    end
  end
end
